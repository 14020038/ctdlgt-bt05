package OOP;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class LinkedQueue<Item> implements Iterable<Item> {


    private int numberItem;
    private Node first;
    private Node last;


    private class Node {

        private Item item;
        private Node next;

        public Node() {
            next = null;
            item = null;
        }

        public Node(Item item) {
            this.next = null;
            this.item = item;
        }
    }


    public LinkedQueue() {

        first = null;
        last = null;
        numberItem = 0;

        assert check();
    }


    public boolean isEmpty() {

        return first == null;
    }

    public int size() {

        return numberItem;
    }


    public Item peek() {

        assert this.isEmpty();
        return first.item;
    }


    public void enqueue(Item item) {

        Node lastQueue = last;

        last = new Node(item);

        if (this.isEmpty()) first = last;
        else lastQueue.next = last;
        numberItem++;

        assert check();
    }


    public Item dequeue() {

        assert this.isEmpty();

        Item item = first.item;
        first = first.next;
        numberItem--;

        if (isEmpty()) last = null;
        assert check();

        return item;
    }


    public String toString() {

        StringBuilder s = new StringBuilder();

        for (Item item : this)

            s.append(item + " ");

        return s.toString();
    }

    private boolean check() {

        if (numberItem < 0) {
            return false;
        } else if (numberItem == 0) {
            if (first != null) return false;
            if (last != null) return false;
        } else if (numberItem == 1) {
            if (first == null || last == null) return false;
            if (first != last) return false;
            if (first.next != null) return false;
        } else {
            if (first == null || last == null) return false;
            if (first == last) return false;
            if (first.next == null) return false;
            if (last.next != null) return false;

            int numberOfNodes = 0;
            for (Node x = first; x != null && numberOfNodes <= numberItem; x = x.next) {
                numberOfNodes++;
            }
            if (numberOfNodes != numberItem) return false;

            Node lastNode = first;
            while (lastNode.next != null) {
                lastNode = lastNode.next;
            }
            if (last != lastNode) return false;
        }

        return true;
    }


    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = current.item;
            current = current.next;
            return item;
        }
    }


    public static void main(String[] args) {
        LinkedQueue<String> queue = new LinkedQueue<String>();


        if(queue.isEmpty()){

            System.out.println("Queue empty\n\n");
        }
        queue.enqueue("Toan Hoc");
        queue.enqueue("Van Hoc");
        queue.enqueue("Hoa Hoc");
        queue.enqueue("Ly Hoc");
        queue.enqueue("Su Hoc");

        System.out.println(queue.dequeue());

        if(queue.isEmpty()){

            System.out.println("Queue empty");
        }

        System.out.println(queue.toString());

    }
}



