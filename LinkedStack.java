package OOP;


import java.util.Iterator;
import java.util.NoSuchElementException;


public class LinkedStack<Item> implements Iterable<Item> {

    private int soPhanTu;
    private Node first;


    private class Node {

        private Item item;
        private Node next;

        public Node() {

        }

        public Node(Item item) {

            this.item = item;
            next = null;
        }
    }


    public LinkedStack() {

        first = null;
        soPhanTu = 0;
        assert check();
    }


    public boolean isEmpty() {
        return first == null;
    }


    public int size() {
        return soPhanTu;
    }


    public void push(Item item) {

        Node old = first;

        first = new Node(item);

        first.next = old;

        soPhanTu++;
        assert check();
    }


    public Item pop() {

        assert this.isEmpty();

        Item item = first.item;
        first = first.next;
        soPhanTu--;

        assert check();
        return item;
    }


    public Item peek() {

        assert this.isEmpty();
        return first.item;
    }


    public String toString() {

        StringBuilder s = new StringBuilder();

        for (Item item : this)

            s.append(item + " ");

        return s.toString();
    }


    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<Item> {

        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = current.item;
            current = current.next;
            return item;
        }
    }


    private boolean check() {

        if (soPhanTu < 0) {
            return false;
        }
        if (soPhanTu == 0) {

            if (first != null) return false;

        } else if (soPhanTu == 1) {
            if (first == null) return false;
            if (first.next != null) return false;
        } else {
            if (first == null) return false;
            if (first.next == null) return false;
        }

        int numberOfNodes = 0;

        for (Node x = first; x != null && numberOfNodes <= soPhanTu; x = x.next) {
            numberOfNodes++;
        }
        if (numberOfNodes != soPhanTu) return false;

        return true;
    }


    public static void main(String[] args) {

        LinkedStack<Integer> s = new LinkedStack<Integer>();

        if (s.isEmpty()) {
            System.out.println("Stack rong");
        }
        s.push(12);
        s.push(23);
        s.push(1);
        s.push(2);
        s.push(4);
        s.push(18);
        s.push(0);

        if (s.isEmpty())

        System.out.println(s.pop());

        System.out.println();

        System.out.println(s.toString());

    }
}



